const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "[name].[chunkhash].js"
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  module: {
    rules: [
      {
	test: /\.css$/,
        use: [
          'style-loader',
	  'css-loader',
	]
      }, {
        test: /\.(png|svg|jpg|gif)$/,
	use: [
          'file-loader',
	]
      }, {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
	use: [
          'file-loader'
	]
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new ManifestPlugin(),
    new UglifyJsPlugin(),
    new HtmlWebpackPlugin({
      title: 'Output Management',
    }),
  ],
};
